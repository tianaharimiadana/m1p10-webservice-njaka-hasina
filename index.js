const express = require("express");
const bodyParser = require("body-parser");
const routes = require("./routes/router");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const cors = require("cors");

dotenv.config();

mongoose.set("strictQuery", true);
mongoose
  .connect(
    "mongodb+srv://mvptm:HSo6nV6g7PVDGkVd@cluster0.kqeqav4.mongodb.net/mvptm?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => console.log("Connexion à MongoDB réussie !"))
  .catch((error) => console.log("Connexion à MongoDB échouée !", error));

const app = express();

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content, Accept, Content-Type, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, DELETE, PATCH, OPTIONS"
  );
  next();
});

app.use(
  cors({
    origin: true,
    credentials: true,
  })
);

app.use(bodyParser.json({ limit: "2000000mb" }));
app.use(bodyParser.urlencoded({ limit: "2000000mb", extended: true }));

routes(app);

app.listen(5000, () => {
  console.log("Running on port 5000.");
});

module.exports = app;
