const mongoose = require("mongoose");

const siteTouristiqueSchema = mongoose.Schema({
  nom_du_site: { type: String, required: true },
  description_du_site: { type: String, required: true },
  categorie: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "SiteCategorie",
    required: true,
  },
  coordonnees_geographiques: { type: [Number], index: "2dsphere" },
  adresse_du_site: { type: String, required: true },
  image_principale_du_site: { type: String, required: true },
  autres_images_du_site: { type: [String] },
  video_du_site: { type: String },
  contenu_html: { type: String },
});

module.exports = mongoose.model("SiteTouristique", siteTouristiqueSchema);
