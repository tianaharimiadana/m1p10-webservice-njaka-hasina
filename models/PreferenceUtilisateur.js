const mongoose = require("mongoose");

const preferenceUtilisateurSchema = mongoose.Schema({
  utilisateur_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Utilisateur",
    required: true,
  },
  categories_preferrees: [
    { type: mongoose.Schema.Types.ObjectId, ref: "SiteCategorie" },
  ],
});

module.exports = mongoose.model(
  "PreferenceUtilisateur",
  preferenceUtilisateurSchema
);
