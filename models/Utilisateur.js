const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const utilisateurSchema = mongoose.Schema({
  nom: { type: String, required: true },
  prenom: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  nom_utilisateur: { type: String, required: true, unique: true },
  mot_de_passe: { type: String, required: true },
});
utilisateurSchema.plugin(uniqueValidator);

module.exports = mongoose.model("Utilisateur", utilisateurSchema);
