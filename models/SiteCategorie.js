const mongoose = require("mongoose");

const siteCategorieSchema = mongoose.Schema({
  nom_categorie: { type: String, required: true },
});

module.exports = mongoose.model("SiteCategorie", siteCategorieSchema);
