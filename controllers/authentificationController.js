const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Utilisateur = require("../models/Utilisateur");

exports.inscription = async (req, res, next) => {
  try {
    const { nom, prenom, email, nom_utilisateur, mot_de_passe } = req.body;
    const utilisateur = new Utilisateur({
      nom: nom,
      prenom: prenom,
      email: email,
      nom_utilisateur: nom_utilisateur,
      mot_de_passe: bcrypt.hashSync(mot_de_passe, 10),
    });

    await utilisateur.save();
    res.status(201).json({
      status: 201,
      message: "Utilisateur créé !",
    });
  } catch (error) {
    res.status(500).json(error);
  }
};

exports.login = async (req, res, next) => {
  try {
    const { nom_utilisateur, mot_de_passe } = req.body;
    const utilisateur = await Utilisateur.findOne({
      nom_utilisateur: nom_utilisateur,
    }).exec();

    if (!utilisateur) {
      throw new Error("Utilisateur non trouvé");
    }

    if (!bcrypt.compareSync(mot_de_passe, utilisateur.mot_de_passe)) {
      throw new Error("Mot de passe incorrect !");
    }

    // Création et envoi du token
    const token = jwt.sign(
      { utilisateurId: utilisateur._id },
      "RANDOM_TOKEN_SECRET",
      { expiresIn: "24h" }
    );
    res.status(200).json({
      data: {
        id: utilisateur._id,
        fullname: utilisateur.nom + " " + utilisateur.prenom,
      },
      token: token,
    });
  } catch (error) {
    res.status(500).json(`${error}`);
  }
};
