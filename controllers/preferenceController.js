const PreferenceUtilisateur = require("../models/PreferenceUtilisateur");

exports.updatePreferencesUtilisateur = async (req, res) => {
  try {
    const utilisateurId = req.params.id;
    const { categories_preferrees } = req.body;
    const preferencesUtilisateur = await PreferenceUtilisateur.findOneAndUpdate(
      { utilisateur_id: utilisateurId },
      { categories_preferrees },
      { new: true, upsert: true }
    );
    res.status(200).json(preferencesUtilisateur);
  } catch (error) {
    res.status(500).json(`${error}`);
  }
};

exports.getPreferencesUtilisateur = async (req, res) => {
  try {
    const utilisateurId = req.params.id;
    const preferencesUtilisateur = await PreferenceUtilisateur.findOne({
      utilisateur_id: utilisateurId,
    })
      .populate("categorie_id", "nom_categorie")
      .exec();
    if (!preferencesUtilisateur) {
      throw new Error("Préférences utilisateur non trouvées.");
    }
    res.status(200).json(preferencesUtilisateur);
  } catch (error) {
    res.status(500).json(`${error}`);
  }
};
