const SiteTouristique = require("../models/SiteTouristique");

exports.getAllSiteTouristique = async (req, res) => {
  try {
    const sites = await SiteTouristique.find()
      .populate("categorie", "nom_categorie")
      .exec();
    res.status(200).json(sites);
  } catch (error) {
    res.status(500).json(`${error}`);
  }
};

exports.getSiteTouristiqueDetails = async (req, res) => {
  try {
    const siteId = req.params.id;
    const siteTouristique = await SiteTouristique.findOne({ _id: siteId })
      .populate("categorie", "nom_categorie")
      .exec();
    if (!siteTouristique) {
      throw new Error("Site touristique introuvable.");
    }
    res.status(200).json(siteTouristique);
  } catch (error) {
    res.status(500).json(`${error}`);
  }
};

exports.addSiteTouristiqueDetails = async (req, res) => {
  try {
    const {
      nom_du_site,
      description_du_site,
      categorie_id,
      coordonnees_geographiques,
      adresse_du_site,
      image_principale_du_site,
      autres_images_du_site,
      video_du_site,
      contenu_html,
    } = req.body;

    const newSite = new SiteTouristique({
      nom_du_site: nom_du_site,
      description_du_site: description_du_site,
      categorie: categorie_id,
      coordonnees_geographiques: coordonnees_geographiques,
      adresse_du_site: adresse_du_site,
      image_principale_du_site: image_principale_du_site,
      autres_images_du_site: autres_images_du_site,
      video_du_site: video_du_site,
      contenu_html: contenu_html,
    });

    await newSite.save();

    res.status(201).json({
      status: 201,
      message: "Site touristique créée !",
    });
  } catch (error) {
    res.status(500).json(`${error}`);
  }
};

exports.findSiteTouristique = async (req, res) => {
  try {
    const { nom_du_site, categorie_id } = req.body;
    const options = {};
    if (nom_du_site) {
      options.nom_du_site = { $regex: nom_du_site, $options: "i" };
    }
    if (categorie_id) {
      options.categorie = categorie_id;
    }
    const sitesTouristiques = await SiteTouristique.find(options);
    res.status(200).json(sitesTouristiques);
  } catch (error) {
    res.status(500).json(`${error}`);
  }
};
