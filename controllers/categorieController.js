const SiteCategorie = require("../models/SiteCategorie");

exports.saveCategorie = async (req, res) => {
  try {
    const { nom_categorie } = req.body;

    const newCategorie = new SiteCategorie({
      nom_categorie: nom_categorie,
    });

    await newCategorie.save();

    res.status(201).json({
      status: 201,
      message: "Categorie créée !",
    });
  } catch (error) {
    res.status(500).json(error);
  }
};

exports.getAllCategorie = async (req, res) => {
  try {
    const sites = await SiteCategorie.find().exec();
    res.status(200).json(sites);
  } catch (error) {
    res.status(500).json(`${error}`);
  }
};
