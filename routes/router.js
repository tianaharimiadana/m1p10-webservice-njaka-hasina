const express = require("express");
const {
  getAllSiteTouristique,
  getSiteTouristiqueDetails,
  findSiteTouristique,
  addSiteTouristiqueDetails,
} = require("../controllers/siteTouristiqueController");
const {
  saveCategorie,
  getAllCategorie,
} = require("../controllers/categorieController");
const {
  inscription,
  login,
} = require("../controllers/authentificationController");
const {
  updatePreferencesUtilisateur,
  getPreferencesUtilisateur,
} = require("../controllers/preferenceController");

const router = express.Router();

const routes = (app) => {
  // Authentification
  router.post("/auth/inscription", inscription);
  router.post("/auth/login", login);

  // Categorie
  router.post("/categorie", saveCategorie);
  router.get("/categorie", getAllCategorie);

  //Site Touristique
  router.get("/sites", getAllSiteTouristique);
  router.get("/sites/:id", getSiteTouristiqueDetails);
  router.post("/sites", addSiteTouristiqueDetails);
  router.get("/sites/recherche", findSiteTouristique);

  // Preferences Utilisateur
  router.put("/utilisateurs/:id/preferences", updatePreferencesUtilisateur);
  router.get("/utilisateurs/:id/preferences", getPreferencesUtilisateur);

  return app.use("/api/v1", router);
};

module.exports = routes;
